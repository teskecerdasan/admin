<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function getAll($page, $perPage)
    {
        $page --;
        $this->db->select("teacher.id AS `teacherId`, teacher.name AS `teacherName`, teacher.nuptk AS `teacherNuptk`, school.id AS `schoolId`, school.name AS `schoolName`");
        $this->db->from('teacher');
        $this->db->limit($perPage, $page);
        $this->db->join('school', 'school.id = teacher.school_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }   
    }

    public function add($data)
    {
        $query = $this->db->insert('teacher', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $this->db->select("teacher.id AS `teacherId`, teacher.name AS `teacherName`, teacher.nuptk AS `teacherNuptk`, teacher.username AS `teacherUsername`, school.id AS `schoolId`, school.name AS `schoolName`");
        $this->db->where('teacher.id', $id);
        $this->db->from('teacher');
        $this->db->join('school', 'school.id = teacher.school_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function edit($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('teacher', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $query = $this->db->delete('teacher', array('id' => $id));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function cekUser($nuptk)
    {
        $this->db->select("*");
        $this->db->where('nuptk', $nuptk);
        $this->db->from('teacher');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function cekStudent($nisn)
    {
        $this->db->select("*");
        $this->db->where('nisn', $nisn);
        $this->db->from('student');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function changePassword($nuptk, $password)
    {
        $this->db->where('nuptk', $nuptk);
        $query = $this->db->update('teacher', array("password" => $password));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function changePasswordStudent($nisn, $password)
    {
        $this->db->where('nisn', $nisn);
        $query = $this->db->update('student', array("password" => $password));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }
}
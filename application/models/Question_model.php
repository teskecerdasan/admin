<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $query = $this->db->insert('question', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll($page, $perPage)
    {
        $page = ($page - 1) * $perPage;
        $this->db->select("question.id AS `questionId`, question.bobot AS `questionBobot`, question.content AS `questionContent`, question.category_id AS `questionCategoryId`, question.favourable AS `questionFavourable`, category.name AS `categoryName`");
        $this->db->from('question');
        $this->db->limit($perPage, $page);
        $this->db->join('category', 'category.id = question.category_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data['results'] = $query->result_object();
            $data['total'] = $this->db->count_all_results("question");
            return $data;
        } else {
            return false;
        }
    }

    public function getById($id)
    {
        $this->db->select("question.id AS `questionId`, question.bobot AS `questionBobot`, question.content AS `questionContent`, question.category_id AS `questionCategoryId`, question.favourable AS `questionFavourable`, category.name AS `categoryName`");
        $this->db->from('question');
        $this->db->join('category', 'category.id = question.category_id');
        $this->db->where("question.id", $id);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }
    }

    public function edit($id, $data)
    {
        $this->db->where("id", $id);
        $updateQuestion = $this->db->update("question", $data);

        if ($updateQuestion == true) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $query = $this->db->delete('question', array('id' => $id));
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

}
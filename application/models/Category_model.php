<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $query = $this->db->insert('category', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll($page, $perPage)
    {
        $page --;
        $this->db->select("id, name, description");
        $query = $this->db->get("category", $page, $perPage);

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }   
    }

    public function getById($id)
    {
        $this->db->select("id, name, description");
        $query = $this->db->get_where("category", array("id" => $id));
        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function edit($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('category', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $query = $this->db->delete('category', array('id' => $id));
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function report($id, $page, $perPage)
    {
        $page = ($page - 1) * $perPage;
        $this->db->select(" statistic.id as `statisticId`, statistic.value as `statisticValue`, student.id as `studentId`, student.username as `studentUsername`, student.name as `studentName`, student.school_id as `studentSchoolId`, student.nisn as `studentNisn`, category.name as `categoryName`, school.name as `schoolName` ");
        $this->db->from('statistic');
        $this->db->where('category_id', $id);
        $this->db->limit($perPage, $page);
        $this->db->join('student', 'student.username = statistic.student_username');
        $this->db->join('school', 'school.id = student.school_id');
        $this->db->join('category', 'category.id = statistic.category_id');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            $data['results'] = $query->result_object();
            $this->db->select(" statistic.id as `statisticId`, statistic.value as `statisticValue`, student.id as `studentId`, student.username as `studentUsername`, student.name as `studentName`, student.school_id as `studentSchoolId`, student.nisn as `studentNisn` ");
            $this->db->from('statistic');
            $this->db->where('category_id', $id);
            $this->db->join('student', 'student.username = statistic.student_username');
            $data['total'] = $this->db->count_all_results();
            return $data;
        } else {
            return false;
        }
    }

}
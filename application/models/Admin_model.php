<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

	public function getPassword($username)
    {
        $this->db->select("password, level");
        $query = $this->db->get_where("admin", array("username" => $username));

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }
    }

    public function getProfile()
    {
        $this->db->select("id, name, username, email, image");
        $query = $this->db->get_where("admin", array("username" => $_SESSION['username']));

        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }

    }

    public function editProfile($data)
    {
        $this->db->where('username', $_SESSION['username']);
        $query = $this->db->update('admin', $data);

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function changePassword($password)
    {
        $this->db->where('username', $_SESSION['username']);
        $query = $this->db->update('admin', array("password" => $password));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }
}

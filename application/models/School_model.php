<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function add($data)
    {
        $query = $this->db->insert('school', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function getAll($page, $perPage)
    {
        $page --;
        $this->db->select("id, name, npsn");
        $query = $this->db->get("school", $page, $perPage);

        if ($query->num_rows() > 0) {
            return $query->result_object();
        } else {
            return false;
        }   
    }

    public function getById($id)
    {
        $this->db->select("id, name, npsn");
        $query = $this->db->get_where("school", array("id" => $id));
        if ($query->num_rows() > 0) {
            return $query->result_object()[0];
        } else {
            return false;
        }   
    }

    public function edit($id, $data)
    {
        $this->db->where('id', $id);
        $query = $this->db->update('school', $data);
        
        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

    public function delete($id)
    {
        $query = $this->db->delete('school', array('id' => $id));

        if ($query == true) {
            return true;
        } else {
            return false;
        }
    }

}
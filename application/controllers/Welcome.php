<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("admin_model", "admin");
	}

	public function index()
	{
		if (isset($_SESSION['username'])) {
			header("Location:" . base_url("admin"));
		}else{
			header("Location:" . base_url("welcome/login"));
		}	
		
	}

	public function login()
	{
		$message = "";
		if (isset($_SESSION['error'])) {
			$message = $_SESSION['error'];
		}
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'message' => $message);
		$this->load->view("pages/login", $data);
	}

	public function doLogin()
	{
		$username = xss_clean($this->input->post("_username"));
		$password = xss_clean($this->input->post("_password"));
		$userData = $this->admin->getPassword($username);
		$passDb = $userData->password;
		
		if (password_verify($password, $passDb) == true) {
			$data = array(
					'username' => $username,
					'type' => $this->input->post("type")
			);
			if ($this->input->post("type") == "admin") {
				$data['level'] = $userData->level;
			}
			$this->session->set_userdata($data);
			header("Location:" . base_url("admin"));
		}else{
			$this->session->set_flashdata('error', 'Wrong password');
			header("Location:" . base_url("welcome/login"));
		}
	}

	public function logout()
	{
		session_destroy();
		header("Location:" . base_url('welcome/login'));
	}
}

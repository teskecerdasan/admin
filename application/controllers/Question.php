<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("category_model", "category");
		$this->load->model("question_model", "question");
		$this->load->model("admin_model", "admin");
    }

    public function index()
    {
        $page = 1;
		$perPage = 12;
		$this->load->library('pagination');
		if (isset($_GET['page'])) {
			$page = xss_clean($this->input->get("page"));
		}
		if (isset($_GET['per-page'])) {
			$perPage = xss_clean($this->input->get("per-page"));
		}

		$question = $this->question->getAll($page, $perPage);
		$total = ceil($question['total'] / $perPage);
		$startNum = ($page - 1 ) * $perPage + 1;
		$profile = $this->admin->getProfile();		
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'question' => $question['results'], 'name' => $profile->name, 'image' => $profile->image, 'num' => $startNum);
		if ($page == $total) {
            $data['endPage'] = true;
        }else{
            $data['endPage'] = false;
		}

		$config['base_url'] = base_url() . 'question';
        $config['total_rows'] = $question['total'];
        $config['per_page'] = $perPage;
        $config['num_links'] = 3;

        $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = '<span aria-hidden="true">First</span>';
        $config['last_link'] = '<span aria-hidden="true">Last</span>';
        $config['next_link'] = '<span aria-hidden="true">Next</span>';
        $config['prev_link'] = '<span aria-hidden="true">Prev</span>';

        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['reuse_query_string'] = true;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $this->pagination->initialize($config);
        $data['links'] =  $this->pagination->create_links();
		

		$this->load->view('part/header', $data);
		$this->load->view('pages/question/home', $data);
		$this->load->view('part/footer', $data);
    }

    public function edit()
	{
		$id = $this->uri->segment(3);
		$profile = $this->admin->getProfile();
		$question = $this->question->getById($id);
		$appName = $this->config->item("appName");
		$category = $this->category->getAll(1, 8);
		$data = array('appName' => $appName, 'question' => $question[0], 'category' => $category, 'idQuestion' => $id, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/question/edit', $data);
		$this->load->view('part/footer', $data);
	}

	public function add()
	{
		$appName = $this->config->item("appName");
		$profile = $this->admin->getProfile();
		$category = $this->category->getAll(1, 8);
		$data = array('appName' => $appName, 'category' => $category, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/question/add', $data);
		$this->load->view('part/footer', $data);
	}

	public function doAdd()
	{
		$data = array(
				'content' => xss_clean($this->input->post("content")),
				'bobot' => xss_clean($this->input->post("bobot")),
				'favourable' => xss_clean($this->input->post("favourable")),
				'category_id' => xss_clean($this->input->post("category_id"))
			);
		$res = $this->question->add($data);
		if ($res == true) {
			header("Location:" . base_url("question?message=success-add"));
		} else {
			header("Location:" . base_url("question?message=error-add"));
		}
	}

	public function doEdit()
	{
		$data = array(
			"content" => xss_clean($this->input->post("content")),
			"bobot" => xss_clean($this->input->post("bobot")),
			'category_id' => xss_clean($this->input->post("category_id")),
			'favourable' => xss_clean($this->input->post("favourable")),
			"updated_at" => date("Y-m-d H:i:s", time()+60*60*7)
		);
		$idQuestion = $this->input->post("idQuestion");
		if ($this->question->edit($idQuestion, $data) == true) {
			header("Location:" . base_url("question?message=success-update"));
		}else{
			header("Location:" . base_url("question?message=error-update"));
		}
	}

	public function doDelete()
	{
		$id = $this->uri->segment(3);
		if ($this->question->delete($id) == true) {
			header("Location:" . base_url("question?message=success-delete"));
		}else{
			header("Location:" . base_url("question?message=error-delete"));
		}
	}

}
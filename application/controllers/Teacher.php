<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("school_model", 'school');
        $this->load->model("teacher_model", 'teacher');
		$this->load->model("admin_model", "admin");
    }

    public function index()
    {
        $page = 1;
		$perPage = 12;
		if (isset($_GET['page'])) {
			$page = xss_clean($this->input->get("page"));
		}
		if (isset($_GET['per-page'])) {
			$perPage = xss_clean($this->input->get("per-page"));
        }
		$profile = $this->admin->getProfile();		        
        $appName = $this->config->item("appName");
        $teacher = $this->teacher->getAll($page, $perPage);
		$data = array('appName' => $appName, 'teacher' => $teacher, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/teacher/home', $data);
		$this->load->view('part/footer', $data);
    }

    public function add()
    {
		$profile = $this->admin->getProfile();		
        $school = $this->school->getAll(1,10);
        $appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'school' => $school, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/teacher/add', $data);
		$this->load->view('part/footer', $data);
    }

    public function edit()
    {
		$profile = $this->admin->getProfile();		
        $id = $this->uri->segment(3);
        $school = $this->school->getAll(1,10);
        $teacher = $this->teacher->getById($id);
        $appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'teacher' => $teacher, 'id' => $id, 'school' => $school, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/teacher/edit', $data);
		$this->load->view('part/footer', $data);
    }

    public function doAdd()
    {
        $password = password_hash($this->input->post("password"), PASSWORD_BCRYPT);
        $data = array(
            'name' => $this->input->post("name"),
            'username' => $this->input->post("username"),
            'password' => $password,
            'nuptk' => $this->input->post("nuptk"),
            'school_id' => $this->input->post("school"),
        );

        if ($this->teacher->add($data) == true) {
        	header("Location:" . base_url("teacher?message=success-add"));
        } else {
            header("Location:" . base_url("teacher?message=error-add"));
        }
    }

    public function doEdit()
    {
        $data = array(
            'name' => $this->input->post("name"),
            'username' => $this->input->post("username"),
            'nuptk' => $this->input->post("nuptk"),
            'school_id' => $this->input->post("school"),
            'updated_at' => date("Y-m-d H:i:s", time()+60*60*7)
        );

        $id = $this->input->post("id");
        if($this->teacher->edit($id, $data) == true){
        	header("Location:" . base_url("teacher?message=success-edit"));
        } else {
            header("Location:" . base_url("teacher?message=error-edit"));
        }
    }

    public function doDelete()
	{
		$id = $this->uri->segment(3);
		if ($this->teacher->delete($id) == true) {
			header("Location:" . base_url("teacher?message=success-delete"));
		}else{
			header("Location:" . base_url("teacher?message=error-delete"));
		}
	}
}
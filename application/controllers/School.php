<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class School extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("school_model", 'school');
		$this->load->model("admin_model", "admin");
    }

    public function index()
    {
        $page = 1;
		$perPage = 12;
		if (isset($_GET['page'])) {
			$page = xss_clean($this->input->get("page"));
		}
		if (isset($_GET['per-page'])) {
			$perPage = xss_clean($this->input->get("per-page"));
        }

		$profile = $this->admin->getProfile();		        
        $appName = $this->config->item("appName");
        $school = $this->school->getAll($page, $perPage);
		$data = array('appName' => $appName, 'school' => $school, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/school/home', $data);
		$this->load->view('part/footer', $data);
    }

    public function add()
    {
		$profile = $this->admin->getProfile();		
        $appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/school/add', $data);
		$this->load->view('part/footer', $data);
    }

    public function edit()
    {
		$profile = $this->admin->getProfile();		
        $id = $this->uri->segment(3);
        $school = $this->school->getById($id);
        $appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'school' => $school, 'id' => $id, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/school/edit', $data);
		$this->load->view('part/footer', $data);
    }

    public function doAddSchool()
    {
        $data = array(
            'name' => $this->input->post("name"), 
            'npsn' => $this->input->post("npsn")
        );
        if($this->school->add($data) == true){
        	header("Location:" . base_url("school?message=success-add"));
        } else {
            header("Location:" . base_url("school?message=error-add"));
        }
    }

    public function doEditSchool()
    {
        $data = array(
            'name' => $this->input->post("name"), 
            'npsn' => $this->input->post("npsn"),
            'updated_at' => date("Y-m-d H:i:s", time()+60*60*7)
        );
        $id = $this->input->post("id");
        if($this->school->edit($id, $data) == true){
        	header("Location:" . base_url("school?message=success-edit"));
        } else {
            header("Location:" . base_url("school?message=error-edit"));
        }
    }

    public function doDeleteSchool()
	{
		$id = $this->uri->segment(3);
		if ($this->school->delete($id) == true) {
			header("Location:" . base_url("school?message=success-delete"));
		}else{
			header("Location:" . base_url("school?message=error-delete"));
		}
	}
}
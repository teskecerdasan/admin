<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model("category_model", "category");
		$this->load->model("question_model", "question");
		$this->load->model("admin_model", "admin");
		$this->load->model("teacher_model", "teacher");
	}

	public function index()
	{
		$category = $this->category->getAll(1, 8);
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'name' => $profile->name, 'image' => $profile->image, "category" => $category);
		$this->load->view('part/header', $data);
		$this->load->view('pages/home', $data);
		$this->load->view('part/footer', $data);
	}

	public function profile()
	{
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'admin' => $profile, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/editProfile', $data);
		$this->load->view('part/footer', $data);
	}

	public function doEditProfile()
	{
		$data = array(
			'name' => xss_clean($this->input->post("name")),
			'email' => xss_clean($this->input->post("email"))
		);

		if ($this->admin->editProfile($data) == true) {
			header("Location:" . base_url("admin?message=update-success"));
		} else {
			header("Location:" . base_url("admin?message=update-error"));
		}
	}

	public function changepasswordUser()
	{
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/changeUserPassword', $data);
		$this->load->view('part/footer', $data);
	}

	public function doCekUser()
	{
		$type;
		$id;
		if (isset($_POST['type'])) {
			$type = $_POST['type'];
		}

		if (isset($_POST['id'])) {
			$id = $_POST['id'];
		}

		if ($type == "teacher") {
			$res = $this->teacher->cekUser($id);	
		}else if($type == "student"){
			$res = $this->teacher->cekStudent($id);
		}

		if ($res == false) {
			echo "false";
		}elseif(count($res) > 0){
			echo "true";
		}
	}

	public function doChangePasswordUser()
	{
		$type;
		$id;
		$password;
		if (isset($_POST['type'])) {
			$type = $_POST['type'];
		}

		if (isset($_POST['id'])) {
			$id = $_POST['id'];
		}

		if (isset($_POST['password'])) {
			$password = $_POST['password'];
		}

		$newPassword = password_hash($password, PASSWORD_BCRYPT);		

		if ($type == "teacher") {
			$res = $this->teacher->changePassword($id, $newPassword);	
		}else if($type == "student"){
			$res = $this->teacher->changePasswordStudent($id, $newPassword);
		}

		if ($res == false) {
			echo "false";
		}elseif($res == true){
			echo "true";
		}
	}

	public function changePassword()
	{
		if (!$_POST) {
			header("Location:" . base_url("admin"));
		}else{
			$username = $_SESSION['username'];
			$oldPassword = xss_clean($this->input->post("oldPass"));
			$userData = $this->admin->getPassword($username);
			$passDb = $userData->password;

			if (password_verify($oldPassword, $passDb) == true) {
				$newPassword = password_hash($this->input->post("newPass"), PASSWORD_BCRYPT);
				if ($this->admin->changePassword($newPassword) == true) {
					echo "true";
				} else {
					echo "false";
				}
			}else{
				echo "Tidak";
			}
		}
	}

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
		$this->load->model("category_model", "category");
		$this->load->model("admin_model", "admin");
		
    }

    public function index()
	{		
		$category = $this->category->getAll(1, 8);
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'category' => $category, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/category/home', $data);
		$this->load->view('part/footer', $data);
	}

	public function detail()
	{
		$id = $this->uri->segment(3);
		$category = $this->category->getById($id);
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'category' => $category, 'name' => $profile->name, 'image' => $profile->image, 'category' => $category);
		$this->load->view('part/header', $data);
		$this->load->view('pages/category/detail', $data);
		$this->load->view('part/footer', $data);
	}

	public function add()
	{
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$data = array('appName' => $appName, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/category/add', $data);
		$this->load->view('part/footer', $data);
	}

	public function edit()
	{
		$profile = $this->admin->getProfile();
		$id = $this->uri->segment(3);
		$appName = $this->config->item("appName");
		$category = $this->category->getById($id);
		$data = array('appName' => $appName, 'category' => $category, 'name' => $profile->name, 'image' => $profile->image);
		$this->load->view('part/header', $data);
		$this->load->view('pages/category/edit', $data);
		$this->load->view('part/footer', $data);
	}

	public function report()
	{
		$page = 1;
		$perPage = 12;
		$this->load->library('pagination');
		if (isset($_GET['page'])) {
			$page = xss_clean($this->input->get("page"));
		}
		if (isset($_GET['per-page'])) {
			$perPage = xss_clean($this->input->get("per-page"));
		}

		$id = $this->uri->segment(3);
		$report = $this->category->report($id, $page, $perPage);
		// die(print_r($report));
		$profile = $this->admin->getProfile();
		$appName = $this->config->item("appName");
		$total = ceil($report['total'] / $perPage);
		$startNum = ($page - 1 ) * $perPage + 1;
		$data = array('appName' => $appName, 'report' => $report['results'], 'name' => $profile->name, 'image' => $profile->image, 'num' => $startNum);
		
		if ($page == $total) {
            $data['endPage'] = true;
        }else{
            $data['endPage'] = false;
		}

		$config['base_url'] = base_url() . 'category/report/' . $id;
        $config['total_rows'] = $report['total'];
        $config['per_page'] = $perPage;
        $config['num_links'] = 3;

        $config['full_tag_open'] = '<nav aria-label="Page navigation"><ul class="pagination">';
        $config['full_tag_close'] = '</ul></nav>';
        $config['first_link'] = '<span aria-hidden="true">First</span>';
        $config['last_link'] = '<span aria-hidden="true">Last</span>';
        $config['next_link'] = '<span aria-hidden="true">Next</span>';
        $config['prev_link'] = '<span aria-hidden="true">Prev</span>';

        $config['cur_tag_open'] = '<li class="active"><a>';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $config['reuse_query_string'] = true;
        $config['use_page_numbers'] = true;
        $config['page_query_string'] = true;
        $config['query_string_segment'] = 'page';
        $this->pagination->initialize($config);
        $data['links'] =  $this->pagination->create_links();

		$this->load->view('part/header', $data);
		$this->load->view('pages/category/report', $data);
		$this->load->view('part/footer', $data);
	}

	public function doAdd()
	{
		$data = array(
			'name' => xss_clean($this->input->post("name")),
			'description' => xss_clean($this->input->post("description"))
		);
		$res = $this->category->add($data);
		if ($res == true) {
			header("Location:" . base_url("category?message=success-add"));
		} else {
			header("Location:" . base_url("category?message=error-add"));
		}
	}

	public function doEdit()
	{
		$data = array(
			'name' => xss_clean($this->input->post("name")), 
			'description' => xss_clean($this->input->post("description")),
			'updated_at' => date("Y-m-d H:i:s", time()+60*60*7)
		);
		$id = $this->input->post("id");
		$res = $this->category->edit($id, $data);
		if ($res == true) {
			header("Location:" . base_url("category?message=success-edit"));
		} else {
			header("Location:" . base_url("category?message=error-edit"));
		}
	}

	public function doDelete()
	{
		$id = $this->uri->segment(3);
		if ($this->category->delete($id) == true) {
			header("Location:" . base_url("category?message=success-delete"));
		}else{
			header("Location:" . base_url("category?message=error-delete"));
		}
	}
}
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Question
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Add Question</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="/question/doAdd">
              <div class="box-body">
                <div class="form-group">
                  <label for="Question">Question</label>
                  <input type="text" class="form-control" id="Question" placeholder="Enter question" name="content" autocomplete="off">
                </div>
                <div class="form-group">
                  <div class="radio">
                      <label>
                      <input type="radio" value="1" name="favourable">
                      Favourable
                      </label>
                  </div>
                  <div class="radio">
                      <label>
                      <input type="radio" value="0" name="favourable">
                      Unfavourable
                      </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="Bobot">Bobot</label><small> ( 1 - 9 )</small>
                  <input type="number" class="form-control" id="Bobot" placeholder="Enter Bobot" name="bobot" min="1" max="9" autocomplete="off">
                </div>
                <!-- radio -->
                <div class="form-group">
                    <?php foreach ($category as $item) { ?>
                        <div class="radio">
                            <label>
                            <input type="radio" id="<?= $item->id ?>" value="<?= $item->id ?>" name="category_id">
                            <?= $item->name ?>
                            </label>
                        </div>
                    <?php } ?>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
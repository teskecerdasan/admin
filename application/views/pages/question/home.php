<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Questions
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Questions</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <a href="/question/add" class="btn btn-primary btn-flat"><b>Add Question</b></a>
              <br><br>
              <table id="example2" class="table table-bordered table-hover">  
                <thead>
                <tr>
                  <th>No</th>
                  <th>Content</th>
                  <th>Favourable</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($question as $item) { ?>
                  <tr>
                    <td><?= $num++ ?></td>
                    <td><?= $item->questionContent?></td>
                    <?php if ($item->questionFavourable == "1") { ?>
                      <td>Yes</td>
                    <?php }else if ($item->questionFavourable == "0") { ?>
                      <td>No</td>
                    <?php } ?>
                    <td><?= $item->categoryName?></td>
                    <td>
                        <!-- <a href="/admin/detailQuestion/<?= $item->questionId ?>" class="btn btn-flat btn-info">Detail</a> -->
                        <a href="/question/edit/<?= $item->questionId ?>" class="btn btn-flat btn-warning">Edit</a>
                        <a href="/question/doDelete/<?= $item->questionId ?>" class="btn btn-flat btn-danger">Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                <th>No</th>
                  <th>Content</th>
                  <th>Favourable</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
              <br>
              <a href="/question/add" class="btn btn-primary btn-flat"><b>Add Question</b></a>
              <div class="text-center">
                <?= $links ?>
              </div>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
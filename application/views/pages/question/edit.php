<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Question
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Question</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="/question/doEdit">
              <div class="box-body">
                <div class="form-group">
                  <label for="Question">Question</label>
                  <input type="text" class="form-control" id="Question" placeholder="Enter question" name="content" value="<?= $question->questionContent ?>">
                </div>
                <div class="form-group">
                <span id="dbFavouravle" style="display:none" ><?= $question->questionFavourable ?></span>
                  <div class="radio">
                      <label>
                      <input type="radio" id="favourable1" value="1" name="favourable">
                      Favourable
                      </label>
                  </div>
                  <div class="radio">
                      <label>
                      <input type="radio" id="favourable0" value="0" name="favourable">
                      Unfavourable
                      </label>
                  </div>
                </div>
                <div class="form-group">
                  <label for="Bobot">Bobot</label><small> ( 1 - 9 )</small>
                  <input type="number" class="form-control" id="Bobot" placeholder="Enter Bobot" name="bobot" min="1" max="9" value="<?= $question->questionBobot ?>">
                </div>

                <div class="form-group">
                    <?php foreach ($category as $item) { ?>
                        <div class="radio">
                            <span id="categoryId" style="display:none" ><?= $question->questionCategoryId ?></span>
                            <label>
                            <input type="radio" id="<?= $item->id ?>" value="<?= $item->id ?>" name="category_id">
                            <?= $item->name ?>
                            </label>
                        </div>
                    <?php } ?>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="text" name="idQuestion" value="<?= $idQuestion ?>" hidden>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script>

    $(function() {
      var id = $("#categoryId").html();
      $("#" + id).attr("checked", "checked");
    });

    $(function() {
      var favourable = $("#dbFavouravle").html();
      $("#favourable" + favourable).attr("checked", "checked");
    });

  </script>
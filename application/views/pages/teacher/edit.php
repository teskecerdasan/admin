<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Teacher
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Teacher</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="/teacher/doEdit">
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?= $teacher->teacherName ?>">
                </div>

                <div class="form-group">
                  <label for="username">Username</label>
                  <input type="text" class="form-control" id="username" placeholder="Enter username" name="username"  value="<?= $teacher->teacherUsername ?>">
                </div>

                <div class="form-group">
                  <label for="nuptk">NUPTK</label>
                  <input type="text" class="form-control" id="nuptk" placeholder="Enter NUPTK" name="nuptk"  value="<?= $teacher->teacherNuptk ?>">
                </div>

                <div class="form-group">
                  <label>School</label>
                  <span style="display:none" id="idSchool"><?= $teacher->schoolId?> </span>
                  <select class="form-control" name="school" id="school">
                    <?php for ($i=0; $i < count($school); $i++) { ?>
                      <option value="<?= $school[$i]->id ?>"><?= $school[$i]->name ?></option>
                    <?php } ?>
                  </select>
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <input type="text" name="id" value="<?= $teacher->teacherId ?>" hidden>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script> 
  var id = $("#idSchool").html();  
  $("#school option[value="+ id +"]").attr('selected','selected');
</script>
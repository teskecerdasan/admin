<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Profile
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Edit Profile</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="/admin/doEditProfile" enctype='multipart/form-data'>
              <div class="box-body">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" value="<?= $admin->name ?>">
                </div>
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?= $admin->email ?>">
                </div>
                <div class="form-group">
                  <label for="image">Profil Image</label>
                  <input type="file" id="image" name="image">
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-default">
                  Change Password
                </button>
                <br><br>
                <button type="submit" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <div class="modal fade" id="modal-default">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Password</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="oldPassword">Old Password</label>
                  <input type="password" class="form-control" id="oldPassword" placeholder="Enter old password" name="oldPassword">
                </div>
                <div class="form-group">
                  <label for="newPassword">New Password</label>
                  <input type="password" class="form-control" id="newPassword" placeholder="Enter new password" name="newPassword">
                </div>
                <div class="form-group">
                  <label for="confirmNewPassword">Confirm New Password</label>
                  <input type="password" class="form-control" id="confirmNewPassword" placeholder="Confirm new password" name="confirmNewPassword">
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="changePassword()">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

  <script>
      function changePassword() {
        var oldPass = $("#oldPassword").val();
        var newPass = $("#newPassword").val();
        var confirmPass = $("#confirmNewPassword").val();

        if (newPass == confirmPass) {         
            $.ajax({
              url : "/admin/changePassword",
              method : "POST",
              data : {
                newPass : newPass,
                oldPass : oldPass,
                confirmPass : confirmPass
              },
              success : function(res){
                if (res == 'true') {
                  alert("Password Changed");
                } else if (res == 'error') {
                  alert("Error");
                }
              }
            })
        }else{
            alert("Confirm password and New password not same");
        }
      }
  </script>
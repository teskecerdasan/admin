<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reset Password User
      </h1>
      <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reset Password User</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <!-- /.box-header -->
            <!-- form start -->
            <form id="formChangePassword" >
              <div class="box-body">
                <div class="form-group">
                  <label for="type">Type</label>&nbsp;&nbsp;&nbsp;
                  <select name="type" id="type">
                    <option value="teacher" selected>Guru</option>
                    <option value="student">Murid</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="nuptk" id="labelId">NUPTK</label>
                  <input type="text" class="form-control" id="id" placeholder="Enter NUPTK" name="nuptk">
                  <br>
                  <button onClick="cekUser()"  class="btn btn-primary">CEK</button>
                </div>

                <div class="form-group">
                  <label for="password">Password</label>
                  <input type="password" class="form-control" id="password" placeholder="Enter password" name="password"  value="">
                </div>

                <div class="form-group">
                  <label for="passwordConfirm">Re-Enter Password</label>
                  <input type="password" class="form-control" id="passwordConfirm" placeholder="Re-Enter Password" name="passwordConfirm"  value="">
                </div>

              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button onClick="changePassword()" class="btn btn-primary">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<script> 
  $("#type").change(function(e){
    var val = e.target.value;  
    if (val == "student") {
      $("#labelId").html("NISN");
      $("#id").attr("placeholder", "Enter NISN");
    }else if(val == "teacher"){
      $("#labelId").html("NUPTK");
      $("#id").attr("placeholder", "Enter NUPTK");
    }
  })

  $("#formChangePassword").submit(function(e){
      e.preventDefault();
  })

  function cekUser(){
    var val = $("#type").val();
    var id = $("#id").val();

    if (id == "" || id == null) {
      alert("Mohon lengkapi isian");
    }else{
      $.ajax({
        url : "<?= base_url('user/doCekUser')?>",
        method : "POST",
        data : {
          type : val,
          id : id
        },
        success : function(res){
          if (res == "true") {
            alert("User ditemukan");
          }else if(res == "false"){
            alert("User tidak ada");
          }
        }
      })
    }

    
  }

  function changePassword(){
    var val = $("#type").val();
    var id = $("#id").val();
    var password = $("#password").val();
    var passwordConfirm = $("#passwordConfirm").val();

    if (val == "" || val == null || id == "" || id == null || password == "" || password == null || passwordConfirm == "" || passwordConfirm == null) {
      alert("Mohon lengkapi isian");
    }else if(password != passwordConfirm){
      alert("Password dengan re-enter password tidak sama");
    }else{
      $.ajax({
        url : "<?= base_url('user/doChangePasswordUser')?>",
        method : "POST",
        data : {
          type : val,
          id : id,
          password : password
        },
        success : function(res){       
          if (res == "true") {
            alert("Berhasil");
          }else if(res == "false"){
            alert("Gagal");
          }
        }
      })
    }
  }

</script>
<!-- Content Wrapper. Contains page content -->
<?php //print_r($report); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Report <?= $report[0]->categoryName ?>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">  
                <thead>
                <tr>
                  <th>No</th>
                  <th>NISN</th>
                  <th>Name</th>
                  <th>Sekolah</th>
                  <th>Nilai</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($report as $item) { ?>
                  <tr>
                    <td><?= $num++ ?></td>
                    <td><?= $item->studentNisn?></td>
                    <td><?= $item->studentName?></td>
                    <td><?= $item->schoolName?></td>
                    <td><?= $item->statisticValue?></td>
                  </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>NISN</th>
                  <th>Name</th>
                  <th>Sekolah</th>
                  <th>Nilai</th>
                </tr>
                </tfoot>
              </table>
              <br>
              <div class="text-center">
                <?= $links ?>
              </div>
            </div>
            <!-- /.box-body -->
            
          </div>
          <!-- /.box -->
          
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
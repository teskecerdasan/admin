<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Category
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Category</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php $n=1; foreach ($category as $item) { ?>
                  <tr>
                    <td><?= $n++ ?></td>
                    <td><?= $item->name?></td>
                    <td>
                        <a href="/category/detail/<?= $item->id ?>" class="btn btn-flat btn-primary">Detail</a>
                        <a href="/category/report/<?= $item->id ?>" class="btn btn-flat btn-primary">Report</a>
                        <a href="/category/edit/<?= $item->id ?>" class="btn btn-flat btn-warning">Edit</a>
                        <a href="/category/doDelete/<?= $item->id ?>" class="btn btn-flat btn-danger">Delete</a>
                    </td>
                  </tr>
                <?php } ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>No</th>
                  <th>Category</th>
                  <th>Action</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
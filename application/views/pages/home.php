  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <?php $n=1; foreach ($category as $item) { ?>
          <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <?php if($n % 4 == 0){ ?>
            <div class="small-box bg-aqua">
            <?php }elseif($n % 3 == 0){ ?>
            <div class="small-box bg-green">
            <?php }elseif($n % 2 == 0){ ?>
            <div class="small-box bg-yellow">
            <?php }elseif($n % 1 == 0){ ?>
            <div class="small-box bg-red">
            <?php } ?>
              <div class="inner">
                <h3><?= "#" . $n++?></h3>

                <p><?= $item->name ?></p>
              </div>
              <div class="icon">
                <i class="ion ion-bag"></i>
              </div>
              <a href="<?= "/category/detail" . "/" . $item->id ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        <?php } ?>
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->